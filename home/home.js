var swiper = new Swiper(".slide-container", {
    slidesPerView: 5,
    spacebetween: 30,
    slidesPerGroupSkip: 5,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  });